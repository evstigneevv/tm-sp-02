package ru.evstigneev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.service.ProjectService;

import java.util.Arrays;
import java.util.Collection;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/projectList", method = RequestMethod.GET)
    public String getProjectList(@NotNull final Model model) {
        @NotNull final Collection<Project> projectList = projectService.findAll();
        model.addAttribute("projects", projectList);
        return "projectList";
    }

    @RequestMapping(value = "/projectView/{id}", method = RequestMethod.GET)
    public String getProject(@PathVariable final String id, @NotNull final Model model) throws Exception {
        @NotNull final Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "projectView";
    }

    @RequestMapping(value = "/projectCreate", method = RequestMethod.GET)
    public String createProjectGet(@ModelAttribute("project") final Project project) {
        return "projectCreate";
    }

    @RequestMapping(value = "/projectCreate", method = RequestMethod.POST)
    public String createProjectPost(@ModelAttribute("name") final String name, @ModelAttribute("description") final String description) {
        projectService.create(name, description);
        return "redirect:/projectList";
    }

    @PostMapping(value = "/projectEdit")
    public String updateProjectPost(@ModelAttribute("id") final String id,
                                    @ModelAttribute("name") final String name,
                                    @ModelAttribute("description") final String description,
                                    @ModelAttribute("dateStart") final String dateStart,
                                    @ModelAttribute("dateFinish") final String dateFinish,
                                    @ModelAttribute("status") final Status status) throws Exception {
        projectService.update(id, name, description, dateStart, dateFinish, status);
        return "redirect:/projectList";
    }

    @GetMapping(value = "/projectEdit/{id}")
    public String updateProjectGet(@PathVariable final String id, @NotNull final Model model) throws Exception {
        @NotNull final Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        model.addAttribute("status", Arrays.asList(Status.values()));
        return "projectEdit";
    }

    @RequestMapping(value = "/projectDelete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable final String id) throws Exception {
        projectService.remove(id);
        return "redirect:/projectList";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }

}
