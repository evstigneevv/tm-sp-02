package ru.evstigneev.tm.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @NotNull
    private String id;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @Column(name = "date_of_creation")
    @NotNull
    private Date dateOfCreation;

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_finish")
    @Nullable
    private Date dateFinish;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(@NotNull final Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(@Nullable final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@Nullable final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(@NotNull final Project project) {
        this.project = project;
    }

}
