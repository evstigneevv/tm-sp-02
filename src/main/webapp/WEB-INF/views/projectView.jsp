<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project</title>
</head>
<%@include file="navbar.jsp" %>
<body>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Project id</th>
        <th>Project name</th>
        <th>Project description</th>
        <th>Project date of creation</th>
        <th>Project start date</th>
        <th>Project finish date</th>
        <th>Project status</th>
        <th>Tasks</th>
        <th>Edit</th>
        <th>Remove</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><c:out value="${project.id}"/></td>
        <td><c:out value="${project.name}"/></td>
        <td><c:out value="${project.description}"/></td>
        <td><c:out value="${project.dateOfCreation}"/></td>
        <td><c:out value="${project.dateStart}"/></td>
        <td><c:out value="${project.dateFinish}"/></td>
        <td><c:out value="${project.status}"/></td>
        <td><a
                href="${pageContext.request.contextPath}/taskList/${project.id}">Tasks</a></td>
        <td><a href="${pageContext.request.contextPath}/projectEdit/${project.id}">Edit</a></td>
        <td><a href="${pageContext.request.contextPath}/projectDelete/${project.id}">Remove</a></td>
    </tr>
    </tbody>
</table>
<a class="btn btn-primary" href="${pageContext.request.contextPath}/taskCreate/${project.id}">Create
    task</a>
</body>
</html>